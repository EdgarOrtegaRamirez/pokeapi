require "hashie"

module PokeAPI
  autoload :Version,            "pokeapi/version"
  autoload :Base,               "pokeapi/base"
  autoload :Type,               "pokeapi/type"
  autoload :DamageRelations,    "pokeapi/damage_relations"
  autoload :Pokemon,            "pokeapi/pokemon"
  autoload :Move,               "pokeapi/move"
  autoload :Ability,            "pokeapi/ability"
  autoload :Stat,               "pokeapi/stat"
  autoload :FlavorTextEntry,    "pokeapi/flavor_text_entry"
  autoload :Species,            "pokeapi/species"
  autoload :EvolutionDetails,   "pokeapi/evolution_details"
  autoload :Chain,              "pokeapi/chain"
  autoload :EvolutionChain,     "pokeapi/evolution_chain"
  autoload :Requester,          "pokeapi/requester"

  module Parser
    autoload :Pokemon,          "pokeapi/parser/pokemon"
    autoload :Species,          "pokeapi/parser/species"
    autoload :EvolutionChain,   "pokeapi/parser/evolution_chain"
    autoload :EvolutionDetails, "pokeapi/parser/evolution_details"
    autoload :Type,             "pokeapi/parser/type"
  end
end
