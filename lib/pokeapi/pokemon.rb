module PokeAPI
  class Pokemon < Base
    property :id
    property :name
    property :abilities, coerce: Array[Ability]
    property :moves, coerce: Array[Move]
    property :types, coerce: Array[Type]
    property :stats, coerce: Array[Stat]
    property :thumbnail
    property :animated_thumbnail
    property :species_id

    class << self
      def find(id)
        response = Requester.pokemon(id)
        new response
      end
    end

    def species
      @species ||= begin
                     response = Requester.species(species_id)
                     Species.new response
                   end
    end
  end
end
