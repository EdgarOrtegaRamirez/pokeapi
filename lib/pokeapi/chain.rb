module PokeAPI
  class Chain < Base
    property :name
    property :evolution_details, coerce: EvolutionDetails
    property :evolves_to, coerce: Array[Chain]
  end
end
