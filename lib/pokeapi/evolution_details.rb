module PokeAPI
  class EvolutionDetails < Base
    property :item
    property :trigger
    property :gender
    property :held_item
    property :known_move
    property :known_move_type
    property :location
    property :min_level
    property :min_happiness
    property :min_beauty
    property :min_affection
    property :needs_overworld_rain
    property :party_species
    property :party_type
    property :relative_physical_stats
    property :time_of_day
    property :trade_species
    property :turn_upside_down
  end
end
