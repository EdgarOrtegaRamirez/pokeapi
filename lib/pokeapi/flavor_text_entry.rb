module PokeAPI
  class FlavorTextEntry < Base
    property :text
    property :language
    property :version
  end
end
