module PokeAPI
  class Species < Base
    property :id
    property :color_name
    property :color_hex
    property :shape
    property :flavor_text_entries, coerce: Array[FlavorTextEntry]
    property :evolution_chain_id

    def evolution_chain
      @evolution_chain ||= begin
                             response = Requester.evolution_chain(evolution_chain_id)
                             EvolutionChain.new(response)
                           end
    end
  end
end
