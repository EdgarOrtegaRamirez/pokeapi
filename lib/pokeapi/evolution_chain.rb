module PokeAPI
  class EvolutionChain < Base
    property :id
    property :chain, coerce: PokeAPI::Chain
  end
end
