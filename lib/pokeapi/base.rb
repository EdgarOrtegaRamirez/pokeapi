module PokeAPI
  class Base < ::Hashie::Dash
    include ::Hashie::Extensions::Dash::Coercion
    include ::Hashie::Extensions::IgnoreUndeclared
  end
end
