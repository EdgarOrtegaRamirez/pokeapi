module PokeAPI
  class DamageRelations < Base
    property :half_damage_from, coerce: Array[Type]
    property :no_damage_from, coerce: Array[Type]
    property :half_damage_to, coerce: Array[Type]
    property :double_damage_from, coerce: Array[Type]
    property :no_damage_to, coerce: Array[Type]
    property :double_damage_to, coerce: Array[Type]
  end
end
