module PokeAPI
  class Stat < Base
    property :name
    property :url
    property :base_stat
    property :effort
  end
end
