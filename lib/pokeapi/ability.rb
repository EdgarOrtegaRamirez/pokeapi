module PokeAPI
  class Ability < Base
    property :name
    property :is_hidden
    property :slot
    property :url

    def hidden?
      is_hidden
    end
  end
end
