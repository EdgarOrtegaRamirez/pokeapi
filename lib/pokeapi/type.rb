module PokeAPI
  class Type < Base
    TYPES = %w(normal fighting flying poison ground rock bug ghost steel fire water grass electric psychic ice dragon dark fairy).freeze
    property :id
    property :name
    property :color_hex
    property :slot
    property :damage_relations, coerce: DamageRelations

    class << self
      def valid?(type)
        TYPES.include?(type.to_s.downcase)
      end

      def find(id)
        response = Requester.type(id)
        new response
      end
    end

    def reload
      response = Requester.type(id)
      update_attributes!(response)
      self
    end
  end
end
