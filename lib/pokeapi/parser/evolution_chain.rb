module PokeAPI
  module Parser
    class EvolutionChain
      def self.parse(data)
        new(data).parse
      end

      def initialize(data)
        @data = data.clone
        @chain = data.delete :chain
      end

      def parse
        {
          id: @data[:id],
          chain: parse_chain(@chain),
        }
      end

      private

      def parse_chain(chain)
        {
          name: chain[:species][:name],
          evolution_details: EvolutionDetails.parse(chain[:evolution_details].first),
          evolves_to: chain[:evolves_to].map { |evolution_chain| parse_chain(evolution_chain) },
        }
      end
    end
  end
end
