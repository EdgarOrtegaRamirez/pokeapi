module PokeAPI
  module Parser
    class Species
      HEX_COLORS = {
        red: '#f05868',
        blue: '#3088f0',
        yellow: '#f0d048',
        green: '#40b868',
        black: '#585858',
        brown: '#b07030',
        purple: '#a868c0',
        gray: '#a0a0a0',
        white: '#f0f0f0',
        pink: '#f890c8',
      }.freeze

      def self.parse(data)
        new(data).parse
      end

      def initialize(data)
        @data = data.clone
        @flavor_text_entries = @data.delete :flavor_text_entries
        @color_name = @data[:color][:name]
      end

      def parse
        {
          id: @data[:id],
          color_name: @color_name,
          color_hex: HEX_COLORS[@color_name.to_sym],
          shape: @data[:shape][:name],
          flavor_text_entries: flavor_text_entries,
          evolution_chain_id: @data[:evolution_chain][:url].split("/").last,
        }
      end

      private

      def flavor_text_entries
        @flavor_text_entries.map do |text_entry|
          {
            text: text_entry[:flavor_text],
            language: text_entry[:language][:name],
            version: text_entry[:version][:name]
          }
        end
      end
    end
  end
end
