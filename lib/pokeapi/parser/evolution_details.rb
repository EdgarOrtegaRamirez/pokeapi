module PokeAPI
  module Parser
    class EvolutionDetails
      def self.parse(data)
        return if data.nil?
        new(data).parse
      end

      def initialize(data)
        @data = data.clone
        @item = @data.delete :item
        @trigger = @data.delete :trigger
        @held_item = @data.delete :held_item
        @location = @data.delete :location
      end

      def parse
        {
          item: item,
          trigger: trigger,
          held_item: held_item,
          gender: @data[:gender],
          known_move: known_move,
          known_move_type: known_move_type,
          location: location,
          min_level: @data[:min_level],
          min_happiness: @data[:min_happiness],
          min_beauty: @data[:min_beauty],
          min_affection: @data[:min_affection],
          needs_overworld_rain: @data[:needs_overworld_rain],
          party_species: @data[:party_species],
          party_type: @data[:party_type],
          relative_physical_stats: @data[:relative_physical_stats],
          time_of_day: time_of_day,
          trade_species: @data[:trade_species],
          turn_upside_down: @data[:turn_upside_down],
        }
      end

      private

      def known_move
        @data[:known_move] && @data[:known_move][:name]
      end

      def known_move_type
        @data[:known_move_type] && @data[:known_move_type][:name]
      end

      def time_of_day
        @data[:time_of_day].empty? ? nil : @data[:time_of_day]
      end

      def item
        @item && @item[:name]
      end

      def trigger
        @trigger && @trigger[:name]
      end

      def held_item
        @held_item && @held_item[:name]
      end

      def location
        @location && @location[:name]
      end
    end
  end
end
