module PokeAPI
  module Parser
    class Type
      HEX_COLORS = {
        normal: '#A8A878',
        fire: '#F08030',
        fighting: '#C03028',
        water: '#6890F0',
        flying: '#A890F0',
        grass: '#78C850',
        poison: '#A040A0',
        electric: '#F8D030',
        ground: '#E0C068',
        psychic: '#F85888',
        rock: '#B8A038',
        ice: '#98D8D8',
        bug: '#A8B820',
        dragon: '#7038F8',
        ghost: '#705898',
        dark: '#705848',
        steel: '#B8B8D0',
        fairy: '#EE99AC'
      }.freeze

      def self.parse(data)
        new(data).parse
      end

      def initialize(data)
        @data = data.clone
        @damage_relations = @data.delete :damage_relations
      end

      def parse
        {
          id: @data[:id],
          name: @data[:name],
          color_hex: HEX_COLORS[@data[:name].to_sym],
          damage_relations: damage_relations
        }
      end

      private

      def damage_relations
        @damage_relations.each_with_object({}) do |(damage_relation, types), hsh|
          hsh[damage_relation] = types.map do |type|
            {
              id: type[:url].split("/").last,
              name: type[:name]
            }
          end
        end
      end
    end
  end
end
