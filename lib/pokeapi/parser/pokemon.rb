module PokeAPI
  module Parser
    class Pokemon
      ANIMATED_THUMBNAIL_SOURCE = "http://pokegifs.surge.sh/"

      def self.parse(data)
        new(data).parse
      end

      def initialize(data)
        @data = data.clone
        @abilities = @data.delete :abilities
        @moves = @data.delete :moves
        @types = @data.delete :types
        @stats = @data.delete :stats
        @thumbnail = @data.delete(:sprites).delete(:front_default)
        @animated_thumbnail = "#{ANIMATED_THUMBNAIL_SOURCE}#{@data[:id]}.gif"
        @species_id = @data[:species][:url].split("/").last
      end

      def parse
        {
          id: @data[:id],
          name: @data[:name],
          abilities: abilities,
          moves: moves,
          types: types,
          stats: stats,
          thumbnail: @thumbnail,
          animated_thumbnail: @animated_thumbnail,
          species_id: @species_id,
        }
      end

      private

      def abilities
        @abilities.map do |ability|
          {
            name: ability[:ability][:name],
            url: ability[:ability][:url],
            is_hidden: ability[:is_hidden],
            slot: ability[:slot],
          }
        end.sort_by { |ability| ability[:slot] }
      end

      def moves
        @moves.map do |move|
          {
            name: move[:move][:name],
            url: move[:move][:url],
          }
        end
      end

      def types
        @types.map do |type|
          {
            name: type[:type][:name],
            slot: type[:slot],
            id: type[:type][:url].split("/").last,
          }
        end.sort_by { |type| type[:slot] }
      end

      def stats
        @stats.map do |stat|
          {
            name: stat[:stat][:name],
            url: stat[:stat][:url],
            base_stat: stat[:base_stat],
            effort: stat[:effort],
          }
        end
      end
    end
  end
end
