require "http"

module PokeAPI
  class Requester
    NotFoundError = Class.new(StandardError)
    TimeoutError = Class.new(StandardError)
    APIError = Class.new(StandardError)

    API_ENDPOINT             = "https://pokeapi.co/api/v2/"
    POKEMON_ENDPOINT         = "pokemon/"
    POKEMON_SPECIES_ENDPOINT = "pokemon-species/"
    EVOLUTION_CHAIN_ENDPOINT = "evolution-chain/"
    TYPES_ENDPOINT           = "type/"

    class << self
      def pokemon(id)
        data = get("#{POKEMON_ENDPOINT}#{id}")
        Parser::Pokemon.parse(data)
      end

      def species(id)
        data = get("#{POKEMON_SPECIES_ENDPOINT}#{id}")
        Parser::Species.parse(data)
      end

      def evolution_chain(id)
        data = get("#{EVOLUTION_CHAIN_ENDPOINT}#{id}")
        Parser::EvolutionChain.parse(data)
      end

      def type(id)
        data = get("#{TYPES_ENDPOINT}#{id}")
        Parser::Type.parse(data)
      end

      def get(path)
        http = HTTP.get("#{API_ENDPOINT}#{path}/")
        fail NotFoundError  if http.code == 404
        fail TimeoutError   if http.code == 504
        fail APIError       if http.code == 500
        JSON.parse(http.to_s, symbolize_names: true)
      end
    end
  end
end
